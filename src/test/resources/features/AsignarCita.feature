# language: es

Característica: : Automatizar la asignación de una cita en el sistema de administración de hospitales


  Escenario: Ingresar información de un nuevo doctor
    Dado que ingreso al sistema de administracion de hospitales
    Cuando diligencio la siguiente información del doctor:
      |nombredoctor|apellidoDoctor|telefonoDoctor|tipoDocumentoDoctor|documentoDoctor|
      |Juan|Perez|5984500|Cédula de ciudadanía|1029292025|
    Entonces el doctor queda guardado en el sistema.

  Escenario: Ingresar información de un nuevo paciente
    Dado que ingreso al sistema de administracion de hospitales
    Cuando diligencio la siguiente información del paciente:
      |nombrePaciente|apellidoPaciente|telefonoPaciente|tipoDocumentoPaciente|documentoPaciente|
      |Camilo|Velez|5984500|Cédula de ciudadanía|102929292|
    Entonces el paciente queda guardado en el sistema.

  Escenario: Agendar cita de forma exitosa
    Dado que ingreso al sistema de administracion de hospitales
    Cuando quiera agendar una cita con la siguiente información:
      |documentoIdentidadPaciente|documentoIdentiddadDoctor|Observaciones|
      |102929292|1029292025|llegar 15 minutos antes|
    Entonces la cita debe agendarse

  Escenario: Agendar cita de forma incorrecta
    Dado que ingreso al sistema de administracion de hospitales
    Cuando quiera agendar una cita con la siguiente información incorrecta:
      |documentoIdentidadPaciente|documentoIdentiddadDoctor|
      ||1029292432|
    Entonces la cita no debe agendarse
