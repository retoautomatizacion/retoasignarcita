package com.reto.ejercicio.runner;


import cucumber.api.CucumberOptions;
import net.serenitybdd.cucumber.CucumberWithSerenity;
import org.junit.runner.RunWith;

@RunWith(CucumberWithSerenity.class)
@CucumberOptions(features ="src/test/resources/features",glue = "com.reto.ejercicio.definitions")

public class AsignarCitaRunner {
}
