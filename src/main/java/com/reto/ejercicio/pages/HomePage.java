package com.reto.ejercicio.pages;

import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.core.pages.WebElementFacade;
import net.thucydides.core.annotations.DefaultUrl;
import org.hamcrest.MatcherAssert;
import static org.hamcrest.Matchers.containsString;

@DefaultUrl("http://automatizacion.herokuapp.com/k/")
public class HomePage extends PageObject {

    @FindBy(xpath = ".//a[@href='appointmentScheduling']")
    public WebElementFacade btnAsignarCita;
    @FindBy(xpath = ".//small")
    public WebElementFacade lblPrincipal;
    @FindBy(xpath = ".//a[@href='addDoctor']")
    public WebElementFacade btnIngresarDoctor;
    @FindBy(xpath = ".//a[@href='addPatient']")
    public WebElementFacade btnIngresarPaciente;
    
    public void ingresarIngresarDoctor() { btnIngresarDoctor.click(); }
    public void ingresarIngresarPaciente() {
        btnIngresarPaciente.click();
    }
    public void ingresarAsignarCita() {
        btnAsignarCita.click();
    }

   // public void verificarIngresoExitoso() {
     //   btnAsignarCita.click();
       // String labelv="Agendar cita";
        //String strMensaje=lblPrincipal.getText();
        //MatcherAssert.assertThat(strMensaje, containsString(labelv));
    //}
}
