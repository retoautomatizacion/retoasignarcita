package com.reto.ejercicio.pages;
import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.core.pages.WebElementFacade;
import net.serenitybdd.core.pages.WebElementState;
import org.hamcrest.MatcherAssert;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

import static org.hamcrest.Matchers.containsString;

public class AsignarCitaPage extends PageObject {

    @FindBy(xpath ="//*[@id=\'datepicker\']")
    public WebElementFacade txtFecha;
    @FindBy(xpath ="//*[@placeholder='Ingrese el documento de identidad del paciente']")
    public WebElementFacade txtDocumentoPaciente;
    @FindBy(xpath ="//*[@placeholder='Ingrese el documento de identidad del doctor']")
    public WebElementFacade txtDocumentoDoctor;
    @FindBy(xpath ="//textarea")
    public WebElementFacade txtObservaciones;
    @FindBy(xpath =".//a[@onclick='submitForm()']")
    public WebElementFacade btnGuardar;
    @FindBy(xpath =".//h3[@class='panel-title']")
    public WebElementFacade lblGuardado;


    public void pacienteExitoso(String datoPrueba){
        txtDocumentoPaciente.click();
        txtDocumentoPaciente.clear();
        txtDocumentoPaciente.sendKeys(datoPrueba);

    }

    public void doctorExitoso(String datoPrueba){
        txtDocumentoDoctor.click();
        txtDocumentoDoctor.clear();
        txtDocumentoDoctor.sendKeys(datoPrueba);
    }

    public void pacienteFallido(String datoPrueba) {
        txtDocumentoPaciente.click();
        txtDocumentoPaciente.clear();
        txtDocumentoPaciente.sendKeys(datoPrueba);
    }
    public void doctorFallido(String datoPrueba) {
        txtDocumentoDoctor.click();
        txtDocumentoDoctor.clear();
        txtDocumentoDoctor.sendKeys(datoPrueba);
    }

    public void guardarInformacion() {
        btnGuardar.click();
    }

    public String ObtenerFecha(){
    LocalDate date = LocalDate.now().plusDays(1);
    DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy");
    return date.format(formatter);
}

 public void ingresarFecha() {
        txtFecha.clear();
        txtFecha.click();
        txtFecha.sendKeys(ObtenerFecha());
        txtObservaciones.click();
    }

    public WebElementState validarCitaExitosa() {
        String labelv="Guardado:";
        String strMensaje=lblGuardado.getText();
        MatcherAssert.assertThat(strMensaje, containsString(labelv));
        return element(lblGuardado);
    }

    public WebElementState validarCitaFallida() {
        String labelv="Error";
        String strMensaje=lblGuardado.getText();
        MatcherAssert.assertThat(strMensaje, containsString(labelv));
        return element(lblGuardado);
    }


}