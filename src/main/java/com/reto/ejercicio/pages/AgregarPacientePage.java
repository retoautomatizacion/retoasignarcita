package com.reto.ejercicio.pages;
import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.core.pages.WebElementFacade;
import net.serenitybdd.core.pages.WebElementState;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

public class AgregarPacientePage extends PageObject {
    @FindBy(xpath =".//input[@name='name']")
    public WebElementFacade txtNombrePaciente;
    @FindBy(xpath =".//input[@name='last_name']")
    public WebElementFacade txtApellidoPaciente;
    @FindBy(xpath =".//input[@name='telephone']")
    public WebElementFacade txtTelefonoPaciente;
    @FindBy(xpath =".//select[@name='identification_type']")
    public WebElementFacade cmbTipoIdentificacion;
    @FindBy(xpath =".//input[@name='identification']")
    public WebElementFacade txtIdentificacionPaciente;
    @FindBy(xpath =".//input[@name='prepaid']")
    public WebElementFacade chkPrepagada;
    @FindBy(xpath =".//a[@onclick='submitForm()']")
    public WebElementFacade btnGuardarPaciente;
    @FindBy(xpath =".//h3[@class='panel-title']")
    public WebElementFacade lblGuardado;

    public void nombrePaciente(String datoPrueba){
        txtNombrePaciente.click();
        txtNombrePaciente.clear();
        txtNombrePaciente.sendKeys(datoPrueba);
    }
    public void apellidoPaciente(String datoPrueba){
        txtApellidoPaciente.click();
        txtApellidoPaciente.clear();
        txtApellidoPaciente.sendKeys(datoPrueba);
    }
    public void telefonoPaciente(String datoPrueba) {
        txtTelefonoPaciente.click();
        txtTelefonoPaciente.clear();
        txtTelefonoPaciente.sendKeys(datoPrueba);
    }
    public void tipoIdentificacion(String datoPrueba) {
        cmbTipoIdentificacion.click();
        cmbTipoIdentificacion.selectByVisibleText(datoPrueba);
    }
    public void identificacionPaciente(String datoPrueba) {
        txtIdentificacionPaciente.click();
        txtIdentificacionPaciente.clear();
        txtIdentificacionPaciente.sendKeys(datoPrueba);
    }
    public void saludPrepagada(String datoPrueba) {
        chkPrepagada.click();
    }

    public void guardarInformacion() {
        btnGuardarPaciente.click();
    }

    public WebElementState validarExistePaciente() {
        lblGuardado.isPresent();
        return element(lblGuardado);
    }

}