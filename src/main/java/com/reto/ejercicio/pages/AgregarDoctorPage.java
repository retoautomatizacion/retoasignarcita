package com.reto.ejercicio.pages;
import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.core.pages.WebElementFacade;
import net.serenitybdd.core.pages.WebElementState;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

public class AgregarDoctorPage extends PageObject {

    @FindBy(xpath ="//*[@id='name']")
    public WebElementFacade txtNombreDoctor;
    @FindBy(xpath ="//*[@id='last_name']")
    public WebElementFacade txtApellidoDoctor;
    @FindBy(xpath ="//*[@id='telephone']")
    public WebElementFacade txtTelefonoDoctor;
    @FindBy(xpath ="//*[@id='identification_type']")
    public WebElementFacade cmbTipoIdentificacion;
    @FindBy(xpath ="//*[@id='identification']")
    public WebElementFacade txtIdentificacionDoctor;
    @FindBy(xpath =".//a[@onclick='submitForm()']")
    public WebElementFacade btnGuardarDoctor;
    @FindBy(xpath =".//h3[@class='panel-title']")
    public WebElementFacade lblGuardado;

    public void nombreDoctor(String datoPrueba){
        txtNombreDoctor.click();
        txtNombreDoctor.clear();
        txtNombreDoctor.sendKeys(datoPrueba);
    }
    public void apellidoDoctor(String datoPrueba){
        txtApellidoDoctor.click();
        txtApellidoDoctor.clear();
        txtApellidoDoctor.sendKeys(datoPrueba);
    }
    public void telefonoDoctor(String datoPrueba) {
        txtTelefonoDoctor.click();
        txtTelefonoDoctor.clear();
        txtTelefonoDoctor.sendKeys(datoPrueba);
    }
    public void tipoIdentificacion(String datoPrueba) {
        cmbTipoIdentificacion.click();
        cmbTipoIdentificacion.selectByVisibleText(datoPrueba);
    }
    public void identificacionDoctor(String datoPrueba) {
        txtIdentificacionDoctor.click();
        txtIdentificacionDoctor.clear();
        txtIdentificacionDoctor.sendKeys(datoPrueba);
    }

    public void guardarInformacion() {
        btnGuardarDoctor.click();
    }

    public void mensajeGuardado() {
        lblGuardado.isPresent();
    }

    public WebElementState validarExisteDoctor() {
        lblGuardado.isPresent();
        return element(lblGuardado);
    }
}