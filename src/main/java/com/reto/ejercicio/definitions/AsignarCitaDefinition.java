package com.reto.ejercicio.definitions;

import com.reto.ejercicio.pages.AsignarCitaPage;
import com.reto.ejercicio.pages.HomePage;
import com.reto.ejercicio.steps.AsignarCitaStep;
import cucumber.api.DataTable;
import cucumber.api.java.es.Cuando;
import cucumber.api.java.es.Dado;
import cucumber.api.java.es.Entonces;
import net.thucydides.core.annotations.Steps;
import java.util.List;

public class AsignarCitaDefinition {
    @Steps
    AsignarCitaStep asignarCitaStep;

    @Dado("^que ingreso al sistema de administracion de hospitales$")
    public void que_ingreso_al_sistema_de_administracion_de_hospitales() throws Exception {
       asignarCitaStep.ingresarSistemaAdministraciónHospitales();
    }

    @Cuando("^diligencio la siguiente información del doctor:$")
    public void diligencio_la_siguiente_información_del_doctor(DataTable dtDatosDoctor) throws Exception {
        List<List<String>> datosdoctor = dtDatosDoctor.raw();
        for (int i = 1; i < datosdoctor.size(); i++) {
            asignarCitaStep.ingresarDoctor(datosdoctor, i);
            try {
                Thread.sleep(5000);
            } catch (InterruptedException e) {
            }
        }
    }
    @Cuando("^diligencio la siguiente información del paciente:$")
    public void diligencio_la_siguiente_información_del_paciente(DataTable dtDatosPaciente) throws Exception {
        List<List<String>> datospaciente = dtDatosPaciente.raw();
        for (int i = 1; i < datospaciente.size(); i++) {
            asignarCitaStep.ingresarPaciente(datospaciente, i);
            try {
                Thread.sleep(5000);
            } catch (InterruptedException e) {
            }
        }
    }
    @Entonces("^el doctor queda guardado en el sistema\\.$")
    public void el_doctor_queda_guardado_en_el_sistema() throws Exception {
     asignarCitaStep.validarDatosDoctorGuardado();
    }

    @Entonces("^el paciente queda guardado en el sistema\\.$")
    public void el_paciente_queda_guardado_en_el_sistema() throws Exception {

    }

    @Cuando("^quiera agendar una cita con la siguiente información:$")
    public void quiera_agendar_una_cita_con_la_siguiente_información(DataTable dtDatacorrecta) throws Exception {
        List<List<String>> datacorrecta = dtDatacorrecta.raw();
        for (int i = 1; i < datacorrecta.size(); i++) {
            asignarCitaStep.asignarCitaExitosa(datacorrecta, i);
            try {
                Thread.sleep(5000);
            } catch (InterruptedException e) {
            }
        }
    }

    @Entonces("^la cita debe agendarse$")
    public void la_cita_debe_agendarse() throws Exception {

    }

    @Cuando("^quiera agendar una cita con la siguiente información incorrecta:$")
    public void quiera_agendar_una_cita_con_la_siguiente_información_incorrecta(DataTable dtDataincorrecta) throws Exception {
            List<List<String>> dataincorrecta = dtDataincorrecta.raw();
          for(int i=1; i<dataincorrecta.size(); i++) {
                asignarCitaStep.asignarCitaFallida(dataincorrecta, i);
                try {
                    Thread.sleep(5000);
                }catch (InterruptedException e) {}
            }
    }

    @Entonces("^la cita no debe agendarse$")
    public void la_cita_no_debe_agendarse() throws Exception {

    }

}