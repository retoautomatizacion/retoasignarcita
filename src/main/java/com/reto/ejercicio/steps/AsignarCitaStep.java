package com.reto.ejercicio.steps;
import com.reto.ejercicio.pages.AgregarPacientePage;
import net.thucydides.core.annotations.Step;
import com.reto.ejercicio.pages.HomePage;
import com.reto.ejercicio.pages.AsignarCitaPage;
import com.reto.ejercicio.pages.AgregarDoctorPage;

import java.util.List;

public class AsignarCitaStep {
    HomePage homePage;
    AgregarDoctorPage agregarDoctorPage;
    AgregarPacientePage agregarPacientePage;
    AsignarCitaPage asignarcitapage;

    @Step
    public void ingresarSistemaAdministraciónHospitales() {
        homePage.open();
    }
    //@Step
    //public void ingresarOpcionAgregarDoctor() {
      //  homePage.ingresarIngresarDoctor();
    //}
    @Step
    public void ingresarDoctor(List<List<String>> data, int id) {
        homePage.ingresarIngresarDoctor();
        agregarDoctorPage.nombreDoctor(data.get(id).get(0).trim());
        agregarDoctorPage.apellidoDoctor(data.get(id).get(1).trim());
        agregarDoctorPage.telefonoDoctor(data.get(id).get(2).trim());
        agregarDoctorPage.tipoIdentificacion(data.get(id).get(3).trim());
        agregarDoctorPage.identificacionDoctor(data.get(id).get(4).trim());
        agregarDoctorPage.guardarInformacion();
    }

    @Step
    public void ingresarOpcionAsignarCita() {
        homePage.open();
        homePage.ingresarAsignarCita();
    }
    @Step
    public void ingresarPaciente(List<List<String>> data, int id) {
        homePage.ingresarIngresarPaciente();
        agregarPacientePage.nombrePaciente(data.get(id).get(0).trim());
        agregarPacientePage.apellidoPaciente(data.get(id).get(1).trim());
        agregarPacientePage.telefonoPaciente(data.get(id).get(2).trim());
        agregarPacientePage.tipoIdentificacion(data.get(id).get(3).trim());
        agregarPacientePage.identificacionPaciente(data.get(id).get(4).trim());
        agregarPacientePage.guardarInformacion();
    }
    @Step
    public void asignarCitaExitosa(List<List<String>> data, int id) {
        homePage.ingresarAsignarCita();
        asignarcitapage.pacienteExitoso(data.get(id).get(0).trim());
        asignarcitapage.doctorExitoso(data.get(id).get(1).trim());
        asignarcitapage.ingresarFecha();
        asignarcitapage.guardarInformacion();
    }
    @Step
    public void asignarCitaFallida(List<List<String>> data, int id) {
        homePage.ingresarAsignarCita();
        asignarcitapage.pacienteFallido(data.get(id).get(0).trim());
        asignarcitapage.doctorFallido(data.get(id).get(1).trim());
        asignarcitapage.ingresarFecha();
        asignarcitapage.guardarInformacion();
    }
    @Step
    public void validarDatosDoctorGuardado() {
        agregarDoctorPage.validarExisteDoctor().shouldBeVisible();
    }
    @Step
    public void validarDatosPacienteGuardado() {
        agregarPacientePage.validarExistePaciente().shouldBeVisible();
    }
    @Step
    public void validarDatosCitaGuardada() {
        asignarcitapage.validarCitaExitosa().shouldBeVisible();
    }
    @Step
    public void validarDatosCitaFallida() {
        asignarcitapage.validarCitaFallida().shouldBeVisible();
    }


}
