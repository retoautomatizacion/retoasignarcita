<html xmlns="http://www.w3.org/1999/xhtml" lang="en"><head>

<meta charset="utf-8" />
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<meta name="viewport" content="width=device-width, initial-scale=1" />
<meta name="description" content="" />
<meta name="author" content="" />
<link href="../resources/img/favicon.ico" rel="shortcut icon" type="image/vnd.microsoft.icon" />
<title>PSL - Agendar Cita</title>

<!-- Bootstrap Core CSS -->
<link href="../resources/css/bootstrap.min.css" rel="stylesheet" />

<!-- Custom CSS -->
<link href="../resources/css/sb-admin.css" rel="stylesheet" />

<!-- Morris Charts CSS -->
<link href="../resources/css/plugins/morris.css" rel="stylesheet" />

<!-- JQUERY-UI CSS -->
<link href="../resources/css/plugins/jquery-ui.min.css" rel="stylesheet" />

<!-- Custom Fonts -->
<link href="../resources/font-awesome-4.1.0/css/font-awesome.min.css" rel="stylesheet" type="text/css" />

<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

<script type="text/javascript"></script><link rel="stylesheet" type="text/css" href="/B1D671CF-E532-4481-99AA-19F420D90332/netdefender/hui/ndhui.css" /></head>

<body style=""><script type="text/javascript" language="javascript" src="/B1D671CF-E532-4481-99AA-19F420D90332/netdefender/hui/ndhui.js?0=0&amp;0=0&amp;0=0"></script>

	<div id="wrapper">

		<!-- Navigation -->
<nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
	<!-- Brand and toggle get grouped for better mobile display -->
	<div class="navbar-header">
		<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
			<span class="sr-only">Toggle navigation</span> <span class="icon-bar"></span>
			<span class="icon-bar"></span> <span class="icon-bar"></span>
		</button>
		<a class="navbar-brand" href="home">Inicio</a>
	</div>
	<!-- Top Menu Items -->
	<!-- Sidebar Menu Items - These collapse to the responsive navigation menu on small screens -->

	<!-- /.navbar-collapse -->
</nav>

		<div id="page-wrapper">

			<div class="container-fluid">
				<!-- Page Heading -->
				<div class="row">
					<div class="col-lg-12">
						<h1 class="page-header">
							<small> Agendar cita</small>
						</h1>
						<!--                         <ol class="breadcrumb"> -->
						<!--                             <li class="active"> -->
						<!--                                 <i class="fa fa-dashboard"></i> Dashboard -->
						<!--                             </li> -->
						<!--                         </ol> -->
					</div>
				</div>
				<div class="panel panel-warning">
					<div class="panel-heading">
						<h3 class="panel-title">Información:</h3>
					</div>
					<div class="panel-body">
						<p>Permite agregar una cita entre un doctor y un paciente. Tanto el doctor como el paciente deben estar previamente creados. Se debe ingresar el documento de identidad de ambos</p>
						<p>Se deben usar fechas mayores a la fecha actual.</p>
						<p>Una vez se intente crear una cita en la siguiente pantalla se presentan los mensajes de éxito o de error, junto con una confirmación de los datos ingresados. </p>
						<p>A menos que se le haya indicado algo diferente, diseñe y automatice los casos de pruebas que considere convenientes para esta pantalla.</p>
				
						</div>
				</div>

				<div class="row">
						<div class="col-md-4">
							<div class="form-group">
								<label for="datepicker">Día de la cita<span class="text-danger">*</span></label> <input type="text" class="form-control hasDatepicker" id="datepicker" />
							</div>
							<div class="form-group">
								<label for="patient-identification">Documento de identidad del paciente<span class="text-danger">*</span>
								</label> <input type="text" class="form-control" placeholder="Ingrese el documento de identidad del paciente" />
							</div>
							<div class="form-group">
								<label for="doctor-identification">Documento de identidad del doctor<span class="text-danger">*</span>
								</label> <input type="text" class="form-control" placeholder="Ingrese el documento de identidad del doctor" />
							</div>
							<div class="form-group">
								<label for="note">Observaciones
								</label> <textarea class="form-control" placeholder="" rows="3"></textarea>
							</div>

							<a style="margin-bottom: 20px;" onclick="submitForm()" class="btn btn-primary pull-right">Guardar</a>
						</div>
				</div>
				<!-- /.row -->

			</div>
			<!-- /.container-fluid -->

		</div>
		<!-- /#page-wrapper -->

	</div>
	<!-- /#wrapper -->

	<!-- jQuery Version 1.11.0 -->
	<script src="../resources/js/jquery-1.11.0.js"></script>

	<!-- Bootstrap Core JavaScript -->
	<script src="../resources/js/bootstrap.min.js"></script>
	
	<script src="../resources/js/plugins/jqueryui/jquery-ui.min.js"></script>
	
	<script src="../resources/js/app/appointment-scheduling.js"></script>
	



<div id="ui-datepicker-div" class="ui-datepicker ui-widget ui-widget-content ui-helper-clearfix ui-corner-all"></div><div id="tf_hui_container" dir="ltr" onselectstart="return false;" class="no-print" style="position: fixed;"><div id="nd_e506252a6b7649eb9640b54befbe7519_status" class="nd_e506252a6b7649eb9640b54befbe7519_status" style="background-position: 0px 0px;"></div><div id="nd_e506252a6b7649eb9640b54befbe7519_title" class="nd_e506252a6b7649eb9640b54befbe7519_title"><span class="notranslate" style="display:block;"><label id="labeltitle"></label></span><label id="labelstatus" class="green">Esta página es segura</label></div><div class="nd_e506252a6b7649eb9640b54befbe7519_separator"></div><a id="nd_e506252a6b7649eb9640b54befbe7519_dragger" class="nd_e506252a6b7649eb9640b54befbe7519_dragger" title="Dragger"></a></div></body></html>